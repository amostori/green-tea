import 'package:flutter/material.dart';
import 'dart:async';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'customerTimePainter.dart';

class PlayerScreen extends StatefulWidget {
  static const String id = 'PlayerScreen';

  final int seconds;

  final String name;

  final String back;
  PlayerScreen({
    this.seconds,
    this.name,
    this.back,
  });
  @override
  _PlayerScreenState createState() => _PlayerScreenState();
}

class _PlayerScreenState extends State<PlayerScreen>
    with TickerProviderStateMixin {
  AnimationController controller;
  Timer timer;
  static AudioPlayer advancedPlayer = new AudioPlayer();
  AudioCache audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  String localFilePath;
  String get timerString {
    Duration duration = controller.duration * controller.value;
    return '${duration.inMinutes}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: widget.seconds),
    );
    audioCache.load('sounds/sound1.mp3');
    startEverything();
  }

  void cancelTimer() {
    if (timer != null) {
      timer.cancel();
      timer = null;
    }
  }

  void startEverything() {
    startTimer();
    controller.reverse(from: controller.value == 0.0 ? 1.0 : controller.value);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    // print('dispose done');
    cancelTimer();
    advancedPlayer.stop();
  }

  void controlEverything() {
    if (controller.isAnimating ||
        advancedPlayer.state == AudioPlayerState.PLAYING) {
      controller.stop();
      controller.reset();
      advancedPlayer.stop();
      cancelTimer();
    } else {
      startEverything();
    }
  }

  void startTimer() {
    timer = Timer(Duration(seconds: widget.seconds),
        () => audioCache.play('sounds/sound1.mp3'));
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return Scaffold(
      backgroundColor: Colors.white10,
      body: AnimatedBuilder(
          animation: controller,
          builder: (context, child) {
            return Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    child: Image.asset(
                      widget.back,
                      fit: BoxFit.cover,
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                    ),
                    height:
                        controller.value * MediaQuery.of(context).size.height,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Align(
                          alignment: FractionalOffset.center,
                          child: AspectRatio(
                            aspectRatio: 1.0,
                            child: Stack(
                              children: <Widget>[
                                Positioned.fill(
                                  child: GestureDetector(
                                    onTap: controlEverything,
                                    child: CustomPaint(
                                        painter: CustomTimerPainter(
                                      animation: controller,
                                      backgroundColor: Colors.white,
                                      color: themeData.indicatorColor,
                                    )),
                                  ),
                                ),
                                Align(
                                  alignment: FractionalOffset.center,
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        widget.name,
                                        style: TextStyle(
                                            fontSize: 20.0,
                                            color: Colors.white),
                                      ),
                                      Text(
                                        timerString,
                                        style: TextStyle(
                                            fontSize: 112.0,
                                            color: Colors.white),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            );
          }),
    );
  }
}

/*
AnimatedBuilder(
animation: controller,
builder: (context, child) {
return FloatingActionButton.extended(
onPressed: () {
if (controller.isAnimating ||
advancedPlayer.state ==
AudioPlayerState.PLAYING) {
controller.stop();
controller.reset();
advancedPlayer.stop();
cancelTimer();
} else {
startEverything();
}
},
icon: Icon(controller.isAnimating
? Icons.pause
    : Icons.play_arrow),
label: Text(
controller.isAnimating ? "Pause" : "Play"));
}),*/
