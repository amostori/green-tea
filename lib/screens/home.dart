import 'package:flutter/material.dart';
import 'package:green_tea_timer/models/modelsList.dart';
import 'package:green_tea_timer/components/myItem.dart';

class HomeScreen extends StatelessWidget {
  static const String id = 'HomeScreen';
  Widget buildBody(BuildContext ctxt, int index) {
    return MyItem(
      name: MODELS_LIST[index].name,
      seconds: MODELS_LIST[index].seconds,
      picture: MODELS_LIST[index].picture,
      timeString: MODELS_LIST[index].timeString,
      back: MODELS_LIST[index].back,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          child: Image.asset(
            'assets/background.png',
            fit: BoxFit.cover,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          body: Padding(
            padding: const EdgeInsets.only(top: 64.0),
            child: ListView.builder(
              itemBuilder: (BuildContext context, int index) =>
                  buildBody(context, index),
              itemCount: MODELS_LIST.length,
            ),
          ),
        ),
      ],
    );
  }
}
