// flutter build apk --split-per-abi
// flutter build appbundle
// µ
// ctrl + shift + alt + j - zaznacz wszystkie podobne
import 'package:flutter/material.dart';
import 'circularTimer.dart';
import 'package:green_tea_timer/screens/home.dart';
import 'package:move_to_background/move_to_background.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        MoveToBackground.moveTaskToBack();
        return false;
      },
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        initialRoute: HomeScreen.id,
        routes: {
          HomeScreen.id: (context) => HomeScreen(),
          PlayerScreen.id: (context) => PlayerScreen(),
        },
      ),
    );
  }
}
