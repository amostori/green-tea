import 'package:flutter/material.dart';
import 'package:green_tea_timer/circularTimer.dart';

class MyItem extends StatelessWidget {
  static const String id = 'MyItem';
  final String name;
  final int seconds;
  final String timeString;
  final String picture;
  final String back;

  const MyItem({
    this.name,
    this.seconds,
    this.picture,
    this.timeString,
    this.back,
  });

  void selectPage(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => PlayerScreen(
              seconds: seconds,
              name: name,
              back: back,
            )));
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => selectPage(context),
      child: Card(
        color: Colors.transparent,
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image(
                  image: AssetImage('assets/$picture'),
                  height: 100,
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Text(
                    name,
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.white70,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  timeString,
                  style: TextStyle(
                    color: Colors.white70,
                    fontSize: 20,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
