import 'model.dart';

const MODELS_LIST = const [
  MyModel(
    name: 'Zielona herbata',
    seconds: 120,
    timeString: '2 min',
    picture: 'tea25.png',
    back: 'assets/teaBack2.png',
  ),
  MyModel(
    name: 'Zielona herbata',
    seconds: 180,
    timeString: '3 min',
    picture: 'tea35.png',
    back: 'assets/teaBack.png',
  ),
  MyModel(
    name: 'Jajka na miękko',
    seconds: 390,
    timeString: '6,5 min',
    picture: 'egg5.png',
    back: 'assets/eggBack.png',
  ),
  /*MyModel(
    name: 'Ryż, kasza',
    seconds: 900,
    timeString: '15 min',
    picture: 'rice5.png',
    back: 'assets/riceBack2.png',
  ),*/
];
