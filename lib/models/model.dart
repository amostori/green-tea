class MyModel {
  final String name;
  final int seconds;
  final String timeString;
  final String picture;

  final String back;
  const MyModel({
    this.name,
    this.seconds,
    this.picture,
    this.timeString,
    this.back,
  });
}
